using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Security.Policy;
using UdonSharp;
using UnityEngine;
using VRC.SDK3.Data;
using VRC.SDK3.StringLoading;
using VRC.SDKBase;
using VRC.Udon.Common.Interfaces;

public class VRCXExample : UdonSharpBehaviour
{
    /// <summary>
    /// The init endpoint for VRCX
    /// </summary>
    private VRCUrl url = new VRCUrl("http://127.0.0.1:22500/vrcx/init");
    private VRCUrl urlGetPos = new VRCUrl("http://127.0.0.1:22500/vrcx/get?key=cubepos");
    public GameObject cube;
    private string connectionKey = null;

    /// <summary>
    /// Parses the response from a VRCX API request and returns the data string if successful.
    /// </summary>
    /// <param name="response">The response string from the API request.</param>
    /// <returns>The data string from the API response if successful, otherwise null.</returns>
    public string ParseVRCXApiResponse(string response)
    {
        if (String.IsNullOrEmpty(response))
        {
            Debug.LogWarning("VRCX API: Response was null.");
            return null;
        }

        if (VRCJson.TryDeserializeFromJson(response, out DataToken result))
        {
            if (result.TokenType == TokenType.DataDictionary)
            {
                DataDictionary data = (DataDictionary)result;

                // Check if error exists
                if (data.TryGetValue("error", out var errorToken) && !errorToken.IsNull)
                {
                    Debug.LogWarning($"VRCX API: Response contained error: {errorToken}");
                    return null;
                }

                // Check if ok is present and true
                if (data.TryGetValue("ok", out var okToken) && !okToken.Boolean)
                {
                    Debug.LogWarning($"VRCX API: Response was not ok but contained no error??? Response: {response}");
                    return null;
                }

                // Check if data exists and return it
                if (data.TryGetValue("data", out var dataToken))
                {
                    return dataToken.String;
                }
                else
                {
                    Debug.LogWarning($"VRCX API: Response did not contain data. Response: {response}");
                }
            }
        }
        else
        {
            Debug.LogWarning($"VRCX API: Failed to parse response. Response: {response}. Result: {result.ToString()}");
        }

        return null;
    }

    /// <summary>
    /// Stores data in VRCX using the provided key and value, and the connection key.
    /// </summary>
    /// <param name="key">The key to store the data under.</param>
    /// <param name="value">The value to store.</param>
    /// <param name="connectionKey">The connection key to use for the request.</param>
    public void StoreVRCXData(string key, string value, string connectionKey)
    {
        DataDictionary dict = new DataDictionary();
        dict.Add("requestType", "store");
        dict.Add("connectionKey", connectionKey);
        dict.Add("key", key);
        dict.Add("value", value);

        if (VRCJson.TrySerializeToJson(dict, JsonExportType.Minify, out DataToken json))
        {
            Debug.Log("[VRCX-World] " + json.String);
        }
        else
        {
            Debug.LogWarning($"Failed to serialize VRCX store request ({json}): {dict.ToString()}");
            return;
        }
    }

    public void Start()
    {

    }

    public void Init()
    {
        VRCStringDownloader.LoadUrl(url, (IUdonEventReceiver)this);
    }

    public override void OnStringLoadSuccess(IVRCStringDownload result)
    {
        Debug.Log($"StringLoad '{result.Url}' Success: {result.Result}");
        string data = ParseVRCXApiResponse(result.Result);
        if (result.Url.Get().Contains("/init"))
        {
            connectionKey = data;
            Debug.Log("Got connection key, " + connectionKey);

            StoreVRCXData("wowatestkey", "look another value for this world", connectionKey);

            DataDictionary dict = new DataDictionary();
            dict.Add("requestType", "store");
            dict.Add("connectionKey", connectionKey);
            dict.Add("key", "testkey");
            dict.Add("value", "look a value for this world");

            // yes you can nest json
            if (VRCJson.TrySerializeToJson(dict, JsonExportType.Minify, out DataToken json))
            {
                StoreVRCXData("look-another-key", json.String, connectionKey);
            }
            else
            {
                Debug.LogWarning($"Failed to serialize VRCX store request ({json}): {dict.ToString()}");
                return;
            }

            return;
        }

        if (result.Url.Get().Contains("key=cubepos"))
        {
            Debug.Log("/get Response Result: " + result.Result);
            Debug.Log("CubePos Returned " + data);
            string[] pos = data.Substring(1, data.Length - 2).Split(',');

            cube.transform.position = new Vector3(float.Parse(pos[0].Trim()), float.Parse(pos[1].Trim()), float.Parse(pos[2].Trim()));
            Debug.Log("Cube position set to " + cube.transform.position);
        }
    }

    public override void OnStringLoadError(IVRCStringDownload result)
    {
        Debug.Log($"VRCX API: StringLoad '{result.Url}' Fail {result.ErrorCode} - '{result.Error}': {result.Result}");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                Debug.Log("Loading cube position from DB");
                VRCStringDownloader.LoadUrl(urlGetPos, (IUdonEventReceiver)this);
            }
            else
            {
                Debug.Log("Storing cube position");
                // Store cube position in VRCX
                StoreVRCXData("cubepos", cube.transform.position.ToString(), connectionKey);
            }
        }

        if (Input.GetKeyDown(KeyCode.J))
        {
            Init();
        }
    }
}
